import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import VisualizarClienteComponent from './cliente/visualizar';
import CadastroClienteComponent from './cliente/cadastro';
import IndexClienteComponent from './cliente';
import LoginComponent from './login/login';
import LogoutComponent from './login/logout';
import { GuardedRoute, GuardProvider } from 'react-router-guards';

/**
 * Auth Guard React
 */

  const requireLogin = (to, from, next) => {    
    localStorage.getItem('@token') ? next() : next.redirect('/login');
  };
 

class App extends Component {
  render() {
    return (
      <Router>
          <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{ color: 'red'}}>
            <Link to={'/list'} className="navbar-brand">Cadastro de Clientes</Link>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              {
                localStorage.getItem('@token') ? 
                  <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                      <Link to={'/list'} className="nav-link">Inicio</Link>
                    </li>
                    <li className="nav-item">
                      <Link to={'/create'} className="nav-link">Cadastro</Link>
                    </li>
                    <li className="nav-item">
                      <Link to={'/logout'} className="nav-link">Sair</Link>
                    </li>
                  </ul> :
                  <ul></ul>
              }
              
            </div>
          </nav> <br/>
          <GuardProvider guards={[requireLogin]} >
          <Switch>
              <GuardedRoute path='/create' exact component={ CadastroClienteComponent } />
              <GuardedRoute path='/edit/:id' exact component={ CadastroClienteComponent } />
              <GuardedRoute path='/view/:id' exact component={ VisualizarClienteComponent } />
              <GuardedRoute path='/list' exact component={ IndexClienteComponent } />
              <GuardedRoute path='/login' exact component={ LoginComponent } />
              <GuardedRoute path='/logout' exact component={ LogoutComponent } />
              <GuardedRoute path='/**' component={ LoginComponent } />
          </Switch>
          </GuardProvider>
      </Router>
    );
  }
}

export default App;
